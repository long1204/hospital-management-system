package com.atguigu.easyexcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;

/**
 * @author xiao
 * @create 2021--08--12 15:24
 */
public class TestRead {
    public static void main(String[] args) {
        String fileName = "H:\\yygh\\excel\\01.xlsx";
        EasyExcel.read(fileName,UserData.class,new ExcelListener()).sheet().doRead();
    }
}
