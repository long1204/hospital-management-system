package com.atguigu.easyexcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import org.apache.poi.sl.usermodel.Sheet;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiao
 * @create 2021--08--12 14:55
 */
public class TestWrite {
    public static void main(String[] args) {
        List<UserData> list = new ArrayList<UserData>();
        for (int i = 0; i < 10; i++){
            UserData userData = new UserData();
            userData.setUid(i);
            userData.setUsername("00" + i);
            list.add(userData);
        }

        String filename = "H:\\yygh\\excel\\01.xlsx";
        EasyExcel.write(filename, UserData.class).sheet("用户信息").doWrite(list);

    }
}
